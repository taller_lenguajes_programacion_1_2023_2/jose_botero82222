print("\n\t* SESION #6 *\n")
#1 imprimir "hola mundo"
print("Hola mundo")
#2 variables y asignacion
nombre="Jose Ricardo"
edad=20
bool=True
#3 tipos de datos basicos
print(type(nombre))
#4 operacion aritmetica
print(9 - 3)
print(6 * 3)
print(14 / 3)
print(int(14 / 3))
#5 conversion entre tipos de datos
edad = str(nombre)
print(edad)
#6  solitar entrada del usuario
entrada=str(input("ingrese su nombre: "))
print(entrada)
#7  condicional if
edad = int(input("digite su edad: "))

if edad>=18:
  print("Usted es mayor de edad")
else:
  print("Usted es menor de edad")

# 8 condicional if else
edad = int(input("digite su edad: "))
if edad>=18:
  print("Usted es mayor de edad")
else:
  print("Usted es menor de edad")

#9 condicioanl if elif else
n1 = float(input("nota uno: "))
n2 = float(input("nota dos: "))
n3 = float(input("nota tres: "))
prom=(n1+n2+n3)/3
if prom<3.0: #si
  print("Usted perdio")
elif prom<3.0 and prom>4.0:  #si no si
  print("Bien estudiante usted gano")
else: #si no
  print("excelente")

#10 bucle for
lista = [1,2,3,4,5,6]
for i in lista:
  print(lista)

num = int(input("Introduce un numero"))
for i in range(5):
  print(num)

#11 bucle while
print("while")
i=5
while i<10:
  num = int(input("digite un numero"))
  i=i+1

#12 uso de break y continue
palabra = input("Introduce una palabra: ")
vocales = ['a', 'e', 'i', 'o', 'u']
for vocales in vocales:
    cont = 0
    for letras in palabra:
        continue
        if letras == vocales:
            cont += 1
        break
    print("La vocal " + vocales + " aparece " + str(cont) + " veces")


#13 listas y operaciones basicas
Lista1 = [1,2,4,6,7,4]
Lista2 = ["hola", "mundo"]
Lista3 = Lista1 + Lista2
print(Lista3)
Lista2.extend('chao')
print(Lista2)
if "hola" in Lista2:
    print("se encuentra la palabra")
else:
    print("no esta")
Lista1.pop(4)
print(len(Lista2)) #longituf de la lista
Lista2.append("bienvenidos")
print(Lista2)
Lista1.insert(3(43))

#14 tuplas y su inmutabilidad
Tupla = (1,2,5,7)
print(max(Tupla))
print(min(Tupla))
print(Tupla.count(2)) # veces que un elemento se encuentra en una tupla

#15 conjuntos y operaciones
Conjunto = {"a", "b", "c"}
Conjunto.add("d")
print(Conjunto)

#16 diccionarios 
puertos ={22:'SSH',23:'Telnet',80:'HTTP',3306:'MySQL'}
print (puertos)
#metodo update,  sirve para unir diccionarios 
dic1 = {22:"SSH",80:"Http"}
dic2 = {53:"DNS",443:"Https"}
print(dic1)
dic1.update(dic2)
#comparar diccionarios
a ={123:"Rojas",87:"Rosas"}=={87:"Rosas",123:"Rojas"}
print(a)
print({"Rosas":123} !={"rosas":123})


b ={123:"Rosas",87:"rojas"}=={"Rosas":123,87:"rojas"}
print(b)
# eliminando elementos del diccionario con el comando del
puertos ={22:"SSH",23:"Telnet",80:"HTTP",3306:"MySQL"}
#print(puertos)
del puertos[23] #diccionario y la clave
print(puertos)
# Metodo dict
#Permite convertir listas de listas  y listas de tuplas a diccionarios

puertos =[[80,"http"],[20,"ftp"],[23,"telnet"]]
d_port =dict(puertos)
print(d_port)

puertos =[(20,"ftp"),(80,"http"),(23,"telnet")]
d_port =dict(puertos)
print(d_port)

#17 list funciones basica
list=[1,23,4,543]
list.append(5)
list.insert(3(24))
list.remove(543)
list.clear(1)
#18 leer un archivo de texto
#19 escribir un archivo de texto
#20 modos de apertura r, w, a, rb, wb
with open (".\Sesiones\Archivos\Clase6.txt", "r") as archivo: #r leer, a añadir, rb imagenes, w escribir
   print(archivo.read())

with open (".\Sesiones\Archivos\Clase6.txt",  "w") as archivo: #r leer, a añadir, rb imagenes, w escribir
   print(archivo.write("Anexo 1, prueba de archivo de texto"))
#------------------------------------------------------------------------------------------------------------
#apertura de archivos json
import json
with open (".vscode\clase.json") as js:
   Archivo=json.load(js)
   print(Archivo)
#crear un archivo json
Persona = {
    "nombre" : "Ricardo",
    "edad" : 20
} 
with open (".vscode\clase6.json", "w") as js:
   json.dump(Persona, js)

#21 trabajo con csv 
import pandas as pd
Documento = pd.read_csv(".\Sesiones\Archivos\ClaseCsv.txt", sep=";")
Filtrar=Documento["Fecha"] == "9/08/2023"
print(Filtrar)
print("FIN")

   
   