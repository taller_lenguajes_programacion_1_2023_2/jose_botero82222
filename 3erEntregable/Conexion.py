import sqlite3
import json

class Conexion:
    def __init__(self):
        self.baseDeDatos = sqlite3.connect("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/3erEntregable/DataBase.sqlite")
        self.apuntador = self.baseDeDatos.cursor()
        with open("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/3erEntregable/Queries.json", "r") as queries:
            self.query = json.load(queries)

    def crearTabla(self, nombre, columnas):
        CrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.apuntador.execute(CrearTabla)
        self.baseDeDatos.commit()

    def insertarDato(self, tabla, columnas, datos):
        InsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.apuntador.executemany(InsertarDatos, datos)
        self.baseDeDatos.commit()

    def verTabla(self, tabla):
        SeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(SeleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido

    def actualizarDatos(self, tabla="", columna="", nuevoDato="", id=0):
        ActualizarDatos = self.query["ActualizarDatos"].format(tabla, columna,nuevoDato,'ID', id)
        self.apuntador.execute(ActualizarDatos)
        self.baseDeDatos.commit()

    def eliminarDatos(self, tabla, id):
        EliminarDatos = self.query["EliminarDatos"].format(tabla,'ID', id)
        self.apuntador.execute(EliminarDatos)
        self.baseDeDatos.commit()
        
    