from django.db import models

# Create your models here.
class Clientes(models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE = models.CharField(max_length=30)
    P_APELLIDO = models.CharField(max_length=30)
    S_APELLIDO = models.CharField(max_length=30)
    TELEFONO = models.CharField(max_length=15, null=True)
    DIRECCION = models.CharField(max_length=256, null=True)
    FECNACIMIENTO = models.DateField()
    CORREO = models.EmailField(default='example@example.com')
    USUARIO = models.CharField(max_length=20)
    CLAVE = models.CharField(max_length=20)
