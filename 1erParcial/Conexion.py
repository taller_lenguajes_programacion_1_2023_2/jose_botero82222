import sqlite3
import json
class Conexion:
    def __init__(self):
        self.BaseDeDatos=sqlite3.connect("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/1erParcial/DataBase.sqlite")
        self.pointer=self.BaseDeDatos.cursor()
        with open("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/1erParcial/Queries.json", "r") as File:
            self.Querys=json.load(File)

    def CrearTabla(self, nombreTabla="",Columnas=""):
        """
            Crea la tabla en la base de datos
        """
        if nombreTabla!="" and Columnas!="":
            info = self.Querys["CrearTabla"].format(nombreTabla,self.Querys[Columnas])
            self.pointer.execute(info)
            self.BaseDeDatos.commit()
            print(f"Base de datos, tabla {nombreTabla} se ha creado correctamente.")
    
    def InsertarDato(self,tabla="",interrogantes="",valores=""): #Crear
        """
            Inserta el dato en cada campo
        """
        if tabla!="" and valores!="":
            info = self.queries["InsertarDato"].format(tabla,interrogantes)
            self.pointer.executemany(info,valores)
            self.BaseDeDatos.commit()

    def LeerDato(self, tabla = "", id=""):
        """
            Lee el dato dependiendo de su clave primaria
        """
        if tabla!= "" and id!="":
            info = self.Querys["LeerDato"].format(tabla,id)
            self.pointer.execute(info)
            self.BaseDeDatos.commit()
            return self.pointer.fetchone()

    def ActualizarDato(self, tabla = "", datoAct = "", datoCambio = "", id = ""):
        """
            Actualiza el dato dependiendo el numero, cambio de dato y clave primaria
        """
        if tabla!= "" and datoAct != "" and datoCambio!="":
            info = self.Querys["ActualizarDato"].format(tabla,datoAct,datoCambio,id)
            self.pointer.execute(info)
            self.BaseDeDatos.commit()
            
    def EliminarDato(self, tabla = "", id = ""):
        """
            Elimina el dato dependiendo su clave primaria
        """
        if tabla!= "" and id!="":
            info = self.Querys["EliminarDato"].format(tabla,id)
            self.pointer.execute(info)
            self.BaseDeDatos.commit()
