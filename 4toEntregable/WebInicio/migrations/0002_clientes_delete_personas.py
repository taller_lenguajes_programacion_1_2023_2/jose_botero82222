# Generated by Django 4.2.7 on 2023-12-02 05:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WebInicio', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clientes',
            fields=[
                ('ID', models.IntegerField(primary_key=True, serialize=False)),
                ('NOMBRE', models.CharField(max_length=30)),
                ('P_APELLIDO', models.CharField(max_length=30)),
                ('S_APELLIDO', models.CharField(max_length=30)),
                ('CORREO', models.EmailField(default='example@example.com', max_length=254)),
                ('USUARIO', models.CharField(max_length=20)),
                ('CLAVE', models.CharField(max_length=20)),
            ],
        ),
        migrations.DeleteModel(
            name='Personas',
        ),
    ]
