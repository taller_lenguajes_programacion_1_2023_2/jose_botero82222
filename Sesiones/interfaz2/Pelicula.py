from Conexion import Conexion

class Pelicula(Conexion):
    def __init__(self, nombre="", duracion="", genero=""):
        Conexion.__init__(self)
        self.nombre = nombre
        self.duracion = duracion
        self.genero = genero
        self.crearTabla("peliculas", "Pelicula")

    def agregarDato(self):
        datos = [
            (f"{self.nombre}",f"{self.duracion}",f"{self.genero}")
        ]
        self.insertarDato("Peliculas",3,datos)

    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("peliculas")


