from Conexion import Conexion
class Usuario(Conexion):
    def __init__(self, nombre="", apellido="", dni="", fechaNacimiento=0, correoElectronico="", telefono=""):
        #Conexion.__init__(self)
        super().__init__
        self.nombre=nombre
        self.apellido=apellido
        self.dni=dni
        self.fechaNacimiento=fechaNacimiento
        self.correoElectronico=correoElectronico
        self.telefono=telefono
        self.id=1
        self.CrearBaseDatosUsuario()

    def CrearBaseDatosUsuario(self):
        self.CrearTabla("Usuario", "ColumnaUsuario")
        
    def AgregarDatosUsuario(self):
        DatosUsuario=[
            (f"{self.nombre}",f"{self.apellido}",f"{self.dni}",f"{self.fechaNacimiento}",f"{self.correoElectronico}", f"{self.telefono}")
        ]
        self.InsertarDatos("Usuario", "?","?","?","?","?","?", DatosUsuario)

    def getNombre(self):
        return self.nombre

    def setNombre(self, nombre):
        self.nombre=nombre

    def getApellido(self):
        return self.apellido

    def setApellido(self, apellido):
        self.apellido=apellido

    def getDni(self):
        return self.dni
    
    def setDni(self, dni):
        self.dni = dni

    def getFechaNacimiento(self):
        return self.fechaNacimiento

    def setFechaNacimiento(self, fechaNacimiento):
        self.fechaNacimiento = fechaNacimiento

    def getCorreoElectronico(self):
        return self.correoElectronico

    def setCorreoElectronico(self, correoElectronico):
        self.correoElectronico=correoElectronico

    def getTelefono(self):
        return self.telefono
    
    def setTelefono(self, telefono):
        self.telefono = telefono
    
    


    
    


