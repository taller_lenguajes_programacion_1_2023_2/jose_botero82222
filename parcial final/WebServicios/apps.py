from django.apps import AppConfig


class WebserviciosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WebServicios'
