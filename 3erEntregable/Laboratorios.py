
from Conexion import Conexion 
class Laboratorio(Conexion):
    def __init__(self, nombre="", ubicacion="", experimento=""):
        Conexion.__init__(self)
        """| CONSTRUCTOR LABS|

        Args:
            nombre (str): Nombre del laboratorio.
            ubicacion (str): ubicacion del laboratorio.
            experimento (str): nombre del experimento.
        """
        self.nombre = nombre
        self.ubicacion = ubicacion
        self.experimento = experimento
        self.CrearBaseDatos()


    #funciones
    def CrearBaseDatos(self):
        self.crearTabla("Labs", "Laboratorio")

    def insertarBaseDatos(self):
        datos = [
            (f"{self.nombre}", f"{self.ubicacion}", f"{self.experimento}")
        ]
        self.insertarDato("Labs",3,datos)

    def leerBaseDatos(self):
        self.Datos=self.verTabla("Labs")

    def eliminarBaseDatos(self, id):
        self.eliminarDatos("Labs", id)




        
