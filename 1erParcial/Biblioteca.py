from Conexion import Conexion
class Biblioteca(Conexion):
    def __init__(self, nombre="", direccion="", telefono="", correoElectronico="", capacidad=0, horario=""):
        #Conexion.__init__(self)
        super().__init__
        self.nombre=nombre
        self.direccion=direccion
        self.telefono=telefono
        self.correoElectronico=correoElectronico
        self.capacidad=capacidad
        self.horario=horario
        self.id=1
        self.CrearBaseDatosBiblioteca()

    def CrearBaseDatosBiblioteca(self):
        self.CrearTabla("Biblioteca", "ColumnaBiblioteca")
        
    def AgregarDatosBiblioteca(self):
        DatosBiblioteca=[
            (f"{self.nombre}",f"{self.direccion}",f"{self.telefono}",f"{self.correoElectronico}",f"{self.capacidad}", f"{self.horario}")
        ]
        self.InsertarDatos("Biblioteca", "?","?","?","?","?","?", DatosBiblioteca)

    def getNombre(self):
        return self.nombre

    def setNombre(self, nombre):
        self.nombre = nombre

    def getDireccion(self):
        return self.direccion

    def setDireccion(self, direccion):
        self.direccion= direccion  

    def getTelefono(self):
        return self.telefono
    
    def setTelefono(self, telefono):
        self.telefono = telefono

    def getCorreoElectronico(self):
        return self.correoElectronico

    def setCorreoElectronico(self, correoElectronico):
        self.correoElectronico = correoElectronico

    def getCapacidad(self):
        return self.capacidad

    def setCapacidad(self, capacidad):
        self.capacidad=capacidad

    def getHorario(self):
        return self.horario
    
    def setHorario(self, horario):
        self.horario = horario   
    
    


    
    


