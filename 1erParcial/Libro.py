from Conexion import Conexion
class Libro(Conexion):
    def __init__(self, titulo="", autor="", editorial="", edicion=0, isbn="", añoPublicacion=0):
        #Conexion.__init__(self)
        super().__init__
        self.titulo=titulo
        self.autor=autor
        self.editorial=editorial
        self.edicion=edicion
        self.isbn=isbn
        self.añoPublicacion=añoPublicacion
        self.id=1
        self.CrearBaseDatosLibro()

    def CrearBaseDatosLibro(self):
        self.CrearTabla("Libro", "ColumnaLibro")
        
    def AgregarDatosLibro(self):
        DatosLibro=[
            (f"{self.titulo}",f"{self.autor}",f"{self.editorial}",f"{self.edicion}",f"{self.isbn}", f"{self.añoPublicacion}")
        ]
        self.InsertarDatos("Libro", "?","?","?","?","?","?", DatosLibro)

    def getTitulo(self):
        return self.titulo

    def setTitulo(self, titulo):
        self.titulo=titulo

    def getAutor(self):
        return self.autor

    def setAutor(self, autor):
        self.autor=autor

    def getEditorial(self):
        return self.editorial
    
    def setEditorial(self, editorial):
        self.editorial = editorial

    def getEdicion(self):
        return self.edicion

    def setEdicion(self, edicion):
        self.edicion = edicion

    def getIsbn(self):
        return self.isbn

    def setIsbn(self, isbn):
        self.isbn=isbn

    def getAñoPublicacion(self):
        return self.añoPublicacion
    
    def setAñoPublicacion(self, añoPublicacion):
        self.añoPublicacion = añoPublicacion
    
    


    
    


