from SesionsSQL import BaseDeDatos

class  TablaPersona(BaseDeDatos):
    def __init__(self):
        super().__init__()

    def CrearTablaPersonas(self):
        self.CrearTabla("Personas", "ColumnaPersonas")

    def AgregarUnDatoP(self, cedula, nombre, apellido):
        Datos=[
            (f"{cedula}", f"{nombre}", f"{apellido}")
        ]
        self.InsertarDatos("Personas", "?,?,?", Datos)
#aqui