from TablaPersonas  import TablaPersona
from TablaEstudios import TablaEstudios
CrearTablaP=TablaPersona()
CrearTablaE=TablaEstudios()
CrearTablaP.CrearTablaPersonas()
CrearTablaE.CrearTablaEstudios()
CrearTablaP.AgregarUnDatoP(115421,  "Ricardo", "Botero")
CrearTablaE.AgregarUnDatoE("programador", "2021-2", "2025-1")

CrearTablaP.AgregarUnDatoP(564625, "Jose", "Valencia")
CrearTablaE.AgregarUnDatoE("Arquitecto", "2020-1", "2024-2")

CrearTablaP.AgregarUnDatoP(788954, "Maria", "Isabel")
CrearTablaE.AgregarUnDatoE("Doctora", "2020-2", "2027-2")

CrearTablaP.AgregarUnDatoP(11320121, "Juan", "García")
CrearTablaE.AgregarUnDatoE("Enfermero", "2020-1", "2024-2")

CrearTablaP.AgregarUnDatoP(995652, "Juan", "Lopez")
CrearTablaE.AgregarUnDatoE("Contador", "2018-1", "2024-2")

