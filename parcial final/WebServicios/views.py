from django.shortcuts import render

# Create your views here.
def paginaDetalles(request):
    return render(request, 'detallesServicios.html')