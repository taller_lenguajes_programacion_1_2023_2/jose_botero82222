#Hijo
from Pelicula import Pelicula
import pandas as pd

class Comedia(Pelicula):
    def __init__(self, titulo="", genero="", duracion=0.0, director="", estreno="", clasificacion="", plataformas=""):
        """
            Atributos:
            titulo (String) : Nombre de la pelicula               
            genero (String) : Genero de la pelicula
            duracion (Float) : duracion de la pelicula
            director (String) : Director creador de la pelicula
            estreno (String) : Fecha en la que se estreno, salio la pelicula
            clasificacion (String) : edad permitida para ver la pelicula
            plataforma (String) : medio de streaming en el que se encuentra o fue estrenada la pelicula 
        """
        super().__init__(titulo, genero, duracion, director, estreno)
        self.clasificacion=clasificacion
        self.plataformas=plataformas
        self.CrearBaseDatosHijo()

    def CrearBaseDatosHijo(self):
        self.CrearTabla("ClaseHijo","Comedia")

    def AgregarBaseDatos(self):
        """ agregar tablas a base de datos
        info:
            se añade los datos en una nueva fila
        """

        DatosHijo=[
            (f"{self.clasificacion}", f"{self.plataformas}")
        ]
        self.InsertarDatos("Hijo","?","?", DatosHijo)


    def AgregarDatos(self):
        """ agregar al excel y a la BaseDeDatos
        info:
            se añade los datos a una hoja nueva y en la BaseDeDatos
        """
        
        self.ClaseHijo = self.LeerHoja("ClaseHijo")
        DatosHijo = pd.DataFrame({
            "ID": [self.id],
            "TITULO": [self.getTitulo()],
            "GENERO": [self.getGenero()],
            "DURACION": [self.getDuracion()],
            "DIRECTOR": [self.getDirector()],
            "ESTRENO": [self.getEstreno()],
            "CLASIFICACION": [self.getClasificacion()],
            "PLATAFORMAS": [self.getPlataformas()]
        })

        self.ClaseHijo = pd.concat([self.ClaseHijo, DatosHijo])

        with pd.ExcelWriter(self.xlsl) as writer:
            self.ClasePadre.to_excel(writer, sheet_name="ClasePadre", index=False)
            self.ClaseHijo.to_excel(writer, sheet_name="ClaseHijo", index=False)

        self.AgregarBaseDatos()

        print("Se agregaron los datos")

    def getExcelHijo(self):
        """ Obtener la hoja de ClaseHijo en el excel 
        
        Info:
            Imprimirá los datos actuales       
        """
        self.ClaseHijo=self.LeerHoja("ClaseHijo")
        if not self.ClaseHijo.empty:
            print()
            print(self.ClaseHijo.to_string(index=False))
        else:
            print("No hay datos....")

    def setClasificacion(self, NuevaClasificacion):
        """ cambia la clasificacion
        NuevaClasificacion:String
        """
        self.clasificacion=NuevaClasificacion
    
    def getClasificacion(self):
        """ obtener clasificacion
        return:
            clasificacion:String
        """
        return self.clasificacion
    
    def setPlataformas(self, NuevaPlataformas):
        """ cambia la Plataforma
        NuevaPlataformas:String
        """
        self.Plataformas=NuevaPlataformas
    
    def getPlataformas(self):
        """ obtener Plataforma
        return:
            Plataformas:String
        """
        return self.Plataformas
       




