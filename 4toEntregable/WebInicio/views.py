# Create your views here.
from django.shortcuts import render, redirect
from .models import Clientes

def paginaInicio(request):
    return render(request,'index.html')

def paginaRegistro(request):
    error=""
    if request.method == 'POST':
        nombre = request.POST['nombre']
        primerapellido = request.POST['papellido']
        segundoapellido = request.POST['sapellido']
        correo = request.POST['correo']
        usuario = request.POST['usuario']
        clave = request.POST['clave']
        try:
            usuarioExistente= Clientes.objects.get(USUARIO=usuario)
            error= "Error: Este usuario ya esta en uso."
        except:
            nuevoCliente = Clientes.objects.create(NOMBRE=nombre, P_APELLIDO=primerapellido, S_APELLIDO=segundoapellido, CORREO=correo, USUARIO=usuario, CLAVE=clave)
            return redirect('/login/')
        
    return render(request, 'registro.html',{
        "error": error
    })

def paginaLogin(request):
    error = ""
    if request.method == 'POST':
        try:
            usuario = request.POST['usuario']
            clave = request.POST['clave']
            verificarUsuario = Clientes.objects.get(USUARIO=usuario)
            if verificarUsuario.CLAVE == clave:
                return redirect('/categorias/')
            else:
                error = "La contraseña ingresada es incorrecta."
        except:
            error = "El usuario no esta registrado."
            
    return render(request, 'login.html', {
        "error":error
    })


