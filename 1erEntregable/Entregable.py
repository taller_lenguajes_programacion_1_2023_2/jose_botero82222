#Ejercicio 2, Escribe un programa que convierta una variable x tipo float con valor 5.75 a un tipo int 
jrbv_x=5.75
jrbv_x=  int(jrbv_x)
print(jrbv_x)
#-------------------------------------------------------------------------------------
#ejercicio 12, Dado un numero n entero, escribe un programa que determine si n es primo o no 
jrbv_n= int(input("Ingrese un número entero positivo: "))
primo = True
for i in range(2, jrbv_n):
    if jrbv_n % i == 0:
      primo = False
    break
if primo:
    print(f"El número {jrbv_n} es primo.")
else:
    print(f"El número {jrbv_n} no es primo.")
#------------------------------------------------------------------------------------
#ejercicio 22, Escribe un programa que elimine el ultimo elemento de una lista.
Lista_jrbv=[2,4,6,8,2,5,8]
print("Esta es la lista", Lista_jrbv)
if Lista_jrbv:
    Lista_jrbv.pop()  
    print("La lista después de eliminar el último elemento:", Lista_jrbv)
else:
    print("lista vacia, no se puede eliminar un elemento")
#--------------------------------------------------------------------------------------------------
#ejercicio 32, Dada una lista, escribe un programa que devuelva una lista con los elementos ordenados de menor a mayor
Lista_jrbv=[2,6,8,34,5,6]
Lista_menorAmayor = sorted(Lista_jrbv)
print("Lista original: ", Lista_jrbv)
print("Lista ordenada: ", Lista_menorAmayor)
#ejercicio 42, simula un archivo csv informacion.csv, escribe un programa que calcule el promedio de una columna llamada edad.
import pandas as pd
import csv

suma_edades = 0
contador = 0
with open("./jose_botero82222/1erEntregable/informacion.csv") as pd:
    reader = csv.DictReader(pd)
    for row in reader:
        try:
            edad = int(row['edad'])
            suma_edades += edad
            contador += 1
        except ValueError:
            print(f"Advertencia: Se encontró un valor no válido en la columna 'edad' en la fila {reader.line_num}")
            
if contador > 0:
    promedio = suma_edades / contador
    print(f"El promedio de la columna 'edad' es: {promedio:f}")
else:
    print("No se encontraron datos válidos en la columna 'edad'.")
#ejercicio 52, escribe un programa que lea un archivo cvs y reemplace un valor espeficico en una columna determinada
import pandas as pd
df = pd.read_csv("./jose_botero82222/1erEntregable/archivo2.csv")
df.loc[df['Edad']== 22, 'edad'] = 35
df.to_csv("./jose_botero82222/1erEntregable/archivo2.csv")
#ejercicio 62, simula un archivo excel ventas.xlxs con multiples hojas, escribe un programa que lea todas las hojas y las combine en un unico dataframe
import pandas as pd
datos_hoja1 = {'Producto': ['A', 'B', 'C'],
               'Venta': [100, 200, 150]}
datos_hoja2 = {'Producto': ['D', 'E', 'F'],
               'Venta': [120, 180, 210]}
with pd.ExcelWriter('./jose_botero82222/1erEntregable/ventas.xlsx', engine='openpyxl') as writer:
    df1 = pd.DataFrame(datos_hoja1)
    df2 = pd.DataFrame(datos_hoja2)
    df1.to_excel(writer, sheet_name='Hoja1', index=False)
    df2.to_excel(writer, sheet_name='Hoja2', index=False)


archivo_excel = pd.ExcelFile('./jose_botero82222/1erEntregable/ventas.xlsx')
hojas_excel = archivo_excel.sheet_names

dataframes = []
for hoja in hojas_excel:
    df = pd.read_excel(archivo_excel, sheet_name=hoja)
    dataframes.append(df)

df_combinado = pd.concat(dataframes, ignore_index=True)
#ejercicio 72, simula un archivo excel, escribe un programa que cunete el numero de filas y columnas del archivo
