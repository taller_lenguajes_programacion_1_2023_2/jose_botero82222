import tkinter as tk
from tkinter import messagebox, ttk
from Pelicula import Pelicula #llamar la pelicula


class Interfaz:

    def __init__(self):
        #crear la ventana y darle el estilo
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("Base de datos pelicula")
        self.ventanaPrincipal.configure(bg="grey")
        self.ventanaPrincipal.resizable(False, False)
        
        self.FrameDatosPelicula = tk.Frame(self.ventanaPrincipal)
        self.FrameDatosPelicula.grid(row=0,column=0, padx=5, pady=5)
        self.FrameDatosPelicula.configure(bg="grey")
        self.FrameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.FrameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.FrameBotonesSuperior.configure(bg="grey")
        self.FrameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.FrameBaseDeDatos.grid(row=2,column=0)
        self.FrameBaseDeDatos.configure(bg="grey")
        self.FrameBotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.FrameBotonesLateral.grid(row=2,column=1,padx=5,pady=5)
        self.FrameBotonesLateral.configure(bg="grey")

        #ingresar datos pelicula a la interfaz
        self.pelicula = Pelicula()
        #llamar las clases de la interfaz 
        self.botonesSuperiores()
        self.camposDeTexto()
        self.crearTabla()
        self.vincularBaseDeDatos()
        self.botonesLaterales()
        self.ventanaPrincipal.mainloop()

    def crearTabla(self):
        self.tablaBaseDeDatos = ttk.Treeview(self.FrameBaseDeDatos, show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "Nombre", "Duracion", "Genero"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("Nombre", text="Nombre")
        self.tablaBaseDeDatos.heading("Duracion", text="Duracion")
        self.tablaBaseDeDatos.heading("Genero", text="Genero")
        self.tablaBaseDeDatos.grid(row=0,column=0)
        
    def camposDeTexto(self):
        self.variableNombre = tk.StringVar()
        self.textoNombre = tk.Label(self.FrameDatosPelicula, text="Nombre", bg="grey")
        self.textoNombre.grid(row=0,column=0)
        self.cuadroNombre = tk.Entry(self.FrameDatosPelicula, textvariable=self.variableNombre)
        self.cuadroNombre.grid(row=0, column=1)

        self.variableDuracion = tk.StringVar()
        self.textoDuracion = tk.Label(self.FrameDatosPelicula, text="Duracion", bg="grey")
        self.textoDuracion.grid(row=1,column=0)
        self.cuadroDuracion = tk.Entry(self.FrameDatosPelicula, textvariable=self.variableDuracion)
        self.cuadroDuracion.grid(row=1, column=1)

        self.variableGenero = tk.StringVar()
        self.textoGenero = tk.Label(self.FrameDatosPelicula, text="Genero", bg="grey")
        self.textoGenero.grid(row=2,column=0)
        self.cuadroGenero = tk.Entry(self.FrameDatosPelicula, textvariable=self.variableGenero)
        self.cuadroGenero.grid(row=2, column=1)

    def botonesSuperiores(self):
        self.botonNuevo = tk.Button(self.FrameBotonesSuperior,text="Nuevo", bg="green", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)

        self.botonGuardar = tk.Button(self.FrameBotonesSuperior,text="Guardar", bg="green",command=self.funcionGuardar)
        self.botonGuardar.grid(row=0, column=1,padx=5)

        self.botonCancelar = tk.Button(self.FrameBotonesSuperior,text="Cancelar", bg="green", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0, column=2,padx=5)

    def funcionGuardar(self):
        self.pelicula.nombre = self.variableNombre.get()
        self.pelicula.duracion = self.variableDuracion.get()
        self.pelicula.genero = self.variableGenero.get()
        self.pelicula.agregarDato()
        self.vincularBaseDeDatos()

    def funcionNuevo(self):
        self.variableNombre.set('')
        self.variableDuracion.set('')
        self.variableGenero.set('')       
        self.cuadroNombre.config(state='normal')
        self.cuadroDuracion.config(state='normal')
        self.cuadroGenero.config(state='normal')     
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
    
    def funcionCancelar(self):
        self.variableNombre.set('')
        self.variableDuracion.set('')
        self.variableGenero.set('')       
        self.cuadroNombre.config(state='disabled')
        self.cuadroDuracion.config(state='disabled')
        self.cuadroGenero.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')

    def vincularBaseDeDatos(self):
        self.pelicula.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.pelicula.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)
        
        messagebox.showinfo("Hola", "Se actualizo")
        
    def botonesLaterales(self):
        self.VariableID = tk.IntVar()
        self.textoID = tk.Label(self.FrameBotonesLateral, text="ID: ")
        self.textoID.grid(row=0, column=0)

        self.cuadroID = tk.Entry(self.FrameBotonesLateral, textvariable=self.VariableID)
        self.cuadroID.grid(row=1, column=0)

        self.botonEditar = tk.Button(self.FrameBotonesLateral, text="Editar", bg="green", command=self.funcionEditar)
        self.botonEditar.grid(row=3,column=0, pady=5)

        self.botonEliminar = tk.Button(self.FrameBotonesLateral, text="Eliminar", bg="green", command=self.funcionEliminar)
        self.botonEliminar.grid(row=4,column=0, pady=5)

    def funcionEditar(self):
        if self.variableNombre.get():
            self.pelicula.actualizarDatos("peliculas", "NOMBRE", self.variableNombre.get(), self.VariableID.get())

        if self.variableDuracion.get():
            self.pelicula.actualizarDatos("peliculas", "DURACION", self.variableDuracion.get(), self.VariableID.get())
            
        if self.variableGenero.get():
            self.pelicula.actualizarDatos("peliculas", "GENERO", self.variableGenero.get(), self.VariableID.get())

        self.vincularBaseDeDatos()
    
    def funcionEliminar(self):
        self.pelicula.eliminarDatos("peliculas", self.VariableID.get())
        self.vincularBaseDeDatos()

aplicacion = Interfaz()