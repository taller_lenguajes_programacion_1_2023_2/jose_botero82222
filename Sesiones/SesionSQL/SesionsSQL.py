import sqlite3
import json
# Query

class BaseDeDatos:
    def __init__(self):   
        self.MiBaseDeDatos=sqlite3.connect("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/Sesiones/SesionSQL/BaseDeDatos.sqlite")
        self.Apuntador=self.MiBaseDeDatos.cursor()
        # Apuntador.execute("CREATE TABLE Personas(Id INTEGER PRIMARY KEY AUTOINCREMENT, Cedulo INTEGER UNIQUE, Nombre VARCHAR(20) NOT NULL, Apellido VARCHAR(40) )") #para llamar el query(crear BD)
        with open("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/Sesiones/SesionSQL/Query.json", "r") as Consultas:
            self.Queries = json.load(Consultas)

    def CrearTabla(self, NombreTabla="", Columnas=""): #se llena con los espacios vacios del query
        if NombreTabla!="" and Columnas!="":
            Info=self.Queries["CrearTabla"].format(NombreTabla, self.Queries[Columnas])
            self.Apuntador.execute(Info)
            self.MiBaseDeDatos.commit()
            print("Base de Datos: la tabla", NombreTabla, "Se ha creado correctamente!")

    def InsertarDatos(self, Tabla="", Interrogantes="", Valores=""):
        if Tabla!="" and Valores!="":
            Info=self.Queries["InsertarDatos"].format(Tabla, Interrogantes)
            self.Apuntador.executemany(Info, Valores)
            self.MiBaseDeDatos.commit()
            print("Base de datos: los datos se agregaron correctamente es: ", Tabla, Interrogantes)



        

