#PROFE LOS DATOS SI SE VEN EN LA INTERFAZ PERO NO SE POR QUE SE DEMORA COMO 5 SEGUNDOS EN APARECER 
import tkinter as tk
from tkinter import messagebox, ttk
from Laboratorios import Laboratorio
from Experimento import Experimento

class Interfaz:
    def __init__(self):
        #ingresar los datos de las clases
        self.laboratorio = Laboratorio()
        self.experimento = Experimento()
        #crear ventana y darle estilo, y separar experimento y laboratorio
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("3er Entregable")
        self.ventanaPrincipal.configure(bg="#6aacbe")
        self.ventanaPrincipal.resizable(False, False)
        self.ventanaPrincipal.iconbitmap("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/3erEntregable/imagenes/icono.ico")


        #Crear frame para el menu de la izquierda(laboratorio)
        self.framelaboratorio = tk.Frame(self.ventanaPrincipal)
        self.framelaboratorio.grid(row=0,column=0,padx=5,pady=5)
        self.framelaboratorio.configure(bg="#6aacbe")
        self.framelaboratoriotitulo = tk.Frame(self.framelaboratorio)
        self.framelaboratoriotitulo.grid(row=0, column=0)
        self.framelaboratoriotitulo.configure(bg="#6aacbe")
        self.labellaboratorio = tk.Label(self.framelaboratoriotitulo, text="Laboratorio", font=("Trebuchet MS",30,"bold"))
        self.labellaboratorio.grid(row=0, column=0)
        self.labellaboratorio.configure(bg="#6aacbe")
        imagenlaboratorio = tk.PhotoImage(file="C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/3erEntregable/imagenes/lab.png")
        imagenPE = imagenlaboratorio.subsample(5, 5) 
        labelimagenlaboratorio = tk.Label(self.framelaboratoriotitulo, image=imagenPE, bg="#6aacbe")
        labelimagenlaboratorio.grid(row=1, column=0)
        #--------------------------------------
        self.frameTablaLabs = tk.Frame(self.framelaboratorio)
        self.frameTablaLabs.grid(row=2,column=0)
        self.datosTablaLabs()
        #---------------------------------------
        self.frameDatosLabs = tk.Frame(self.framelaboratorio)
        self.frameDatosLabs.grid(row=3,column=0)
        self.frameDatosLabs.configure(bg="#6aacbe")
        self.campoTextoLabs()
        #----------------------------------------------------
        self.frameBotonesLabs = tk.Frame(self.framelaboratorio)
        self.frameBotonesLabs.grid(row=4,column=0)
        self.frameBotonesLabs.configure(bg="#6aacbe")
        self.botonesLab()
        self.vincularBaseDatosLabs()
        self.frameBotonInferiorLabs = tk.Frame(self.framelaboratorio)
        self.frameBotonInferiorLabs.grid(row=5, column=0)
        self.frameBotonInferiorLabs.configure(bg="#6aacbe")
        self.botonEliminarLabs()

        #separador para los dos menus
        separador = ttk.Separator(self.ventanaPrincipal, orient="vertical")
        separador.grid(row=0, column=1, sticky="ns")

    
        #Crear frame para el menu de la derecha (Experimentos)
        self.frameexperimentos = tk.Frame(self.ventanaPrincipal)
        self.frameexperimentos.grid(row=0,column=2, padx=5, pady=5)
        self.frameexperimentostitulo = tk.Frame(self.frameexperimentos)
        self.frameexperimentostitulo.grid(row=0, column=0)
        self.frameexperimentostitulo.configure(bg="#bddde4")
        self.frameexperimentos.configure(bg="#bddde4")
        self.labelExperimentos = tk.Label(self.frameexperimentostitulo,text="Experimentos", font=("Trebuchet MS",30,"bold"))
        self.labelExperimentos.grid(row=0,column=0)
        self.labelExperimentos.configure(bg="#bddde4")
        imagenexperimento = tk.PhotoImage(file="C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/3erEntregable/imagenes/exp.png")
        imagenEXP = imagenexperimento.subsample(1, 1)
        labelfotoexperimentos = tk.Label(self.frameexperimentostitulo,image=imagenEXP, bg="#bddde4")
        labelfotoexperimentos.grid(row=1, column=0)
        self.frameTablaExperiments = tk.Frame(self.frameexperimentos)
        self.frameTablaExperiments.grid(row=2, column=0)
        self.frameTablaExperiments.configure(bg="#bddde4")
        self.datosTablaExperiments()
        #----------------------------------------------------
        self.frameDatosExps = tk.Frame(self.frameexperimentos)
        self.frameDatosExps.grid(row=3, column=0)
        self.frameDatosExps.configure(bg="#bddde4")
        self.campoTextoExps()
        #----------------------------------------------------
        self.frameBotonesExps = tk.Frame(self.frameexperimentos)
        self.frameBotonesExps.grid(row=4, column=0)
        self.frameBotonesExps.configure(bg="#bddde4")
        self.botonesExps()
        self.vincularBaseDatosExps()
        self.frameBotonInferior = tk.Frame(self.frameexperimentos)
        self.frameBotonInferior.grid(row=5, column=0)
        self.frameBotonInferior.configure(bg="#bddde4")
        self.BotonesInferiores()



        #llamar clases y ejecutar

        self.barraDeMenu()
        self.ventanaPrincipal.mainloop()
        
    #barra de menu en la parte superior :)
    def barraDeMenu(self):
        menuNavegacion = tk.Menu()   
        subMenuInicio = tk.Menu(tearoff=0)
        subMenuLaboratorios = tk.Menu(tearoff=0)
        subMenuExperimentos = tk.Menu (tearoff=0)
        subMenuAyuda = tk.Menu(tearoff=0)
        menuNavegacion.add_cascade(label='Inicio',menu=subMenuInicio)
        menuNavegacion.add_cascade(label='Laboratorios',menu=subMenuLaboratorios)
        menuNavegacion.add_cascade(label='Experimentos',menu=subMenuExperimentos)
        menuNavegacion.add_cascade(label='Ayuda',menu=subMenuAyuda)
        self.ventanaPrincipal.config(menu=menuNavegacion)
        #Submenus
        subMenuInicio.add_command(label='Salir')
        subMenuLaboratorios.add_command(label='Ver Laboratorios')
        subMenuExperimentos.add_command(label='Ver Experimentos')
        subMenuAyuda.add_command(label='Manual del Usuario')

    #LABORATORIO
    #datos de laboratorio
    def datosTablaLabs(self):
        self.tablaLabs = ttk.Treeview(self.frameTablaLabs, show="headings")
        self.tablaLabs.config(columns=("ID", "NOMBRE", "UBICACION", "EXPERIMENTO"))
        self.tablaLabs.heading("ID", text="ID")
        self.tablaLabs.heading("NOMBRE", text="Nombre")
        self.tablaLabs.heading("UBICACION", text="Ubicación")
        self.tablaLabs.heading("EXPERIMENTO", text="Experimento")
        self.tablaLabs.grid(row=0,column=0)
    #cuadros para ingresar los datos de laboratorio(campos de texto)
    def campoTextoLabs(self):
        self.variableNombre = tk.StringVar()
        self.textoLabsNombre = tk.Label(self.frameDatosLabs, text="Nombre", bg="#6aacbe")
        self.textoLabsNombre.grid(row=0, column=0)
        self.cuadroLabsNombre = tk.Entry(self.frameDatosLabs, textvariable=self.variableNombre)
        self.cuadroLabsNombre.grid(row=0,column=1)
        self.cuadroLabsNombre.configure(bg="#6aacbe")

        self.variableUbicacion = tk.StringVar()
        self.textoLabsUbicacion = tk.Label(self.frameDatosLabs, text="Ubicación", bg="#6aacbe")
        self.textoLabsUbicacion.grid(row=1, column=0)
        self.cuadroLabsUbicacion = tk.Entry(self.frameDatosLabs, textvariable=self.variableUbicacion)
        self.cuadroLabsUbicacion.grid(row=1, column=1)
        self.cuadroLabsUbicacion.configure(bg="#6aacbe")

        self.variableExperimento = tk.StringVar()
        self.textoLabsExperimento = tk.Label(self.frameDatosLabs, text="Experimento", bg="#6aacbe")
        self.textoLabsExperimento.grid(row=2, column=0)
        self.cuadroLabsExperimento = tk.Entry(self.frameDatosLabs, textvariable=self.variableExperimento)
        self.cuadroLabsExperimento.grid(row=2, column=1)
        self.cuadroLabsExperimento.configure(bg="#6aacbe")

    #botones guardar, cancelar y nuevo de laboratorio
    def botonesLab(self):
        self.buttonGuardarLab = tk.Button(self.frameBotonesLabs, text="Guardar", command=self.funcionGuardarLabs)
        self.buttonGuardarLab.grid(row=0, column=0, padx=10)
        self.buttonGuardarLab.config(relief="sunken", bd=3, bg="#606060")
        self.buttonCancelarLab = tk.Button(self.frameBotonesLabs, text="Cancelar", command=self.funcionCancelarLabs)
        self.buttonCancelarLab.grid(row=0, column=1,padx=10)
        self.buttonCancelarLab.config(relief="sunken", bd=3, bg="#606060")
        self.buttonNuevoLab = tk.Button(self.frameBotonesLabs, text="Nuevo", command=self.funcionNuevoLabs)
        self.buttonNuevoLab.grid(row=0, column=2, padx=10)
        self.buttonNuevoLab.config(relief="sunken", bd=3, bg="#606060")
    
    #funcion de los botones de laboratorio
    def funcionNuevoLabs(self):
        self.variableNombre.set('')
        self.variableUbicacion.set('')
        self.variableExperimento.set('')
        self.cuadroLabsNombre.config(state='normal')
        self.cuadroLabsUbicacion.config(state='normal')
        self.cuadroLabsExperimento.config(state='normal')
    
    def funcionCancelarLabs(self):
        self.variableNombre.set('')
        self.variableUbicacion.set('')
        self.variableExperimento.set('')
        self.cuadroLabsNombre.config(state='disabled')
        self.cuadroLabsUbicacion.config(state='disabled')
        self.cuadroLabsExperimento.config(state='disabled')

    def funcionGuardarLabs(self):
        self.laboratorio.nombre = self.variableNombre.get()
        self.laboratorio.ubicacion = self.variableUbicacion.get()
        self.laboratorio.experimento = self.variableExperimento.get()
        self.vincularBaseDatosLabs()
        self.laboratorio.insertarBaseDatos()

    #EXPERIMENTO   
    #datos experimento
    def datosTablaExperiments(self):
        self.tablaExps = ttk.Treeview(self.frameTablaExperiments, show="headings")
        self.tablaExps.config(columns=("ID", "NOMBRE", "PROPOSITO", "DURACION", "CIENTIFICO"))
        self.tablaExps.heading("ID", text="ID")
        self.tablaExps.heading("NOMBRE", text="Nombre")
        self.tablaExps.heading("PROPOSITO", text="Propositos")
        self.tablaExps.heading("DURACION", text="Duracion")
        self.tablaExps.heading("CIENTIFICO", text="Cientifico")
        self.tablaExps.grid(row=0,column=0)
    #cuadros para ingresar los datos de experimento(campos de texto)
    def campoTextoExps(self):
        self.variableNombreExps = tk.StringVar()
        self.textoExpsNombre = tk.Label(self.frameDatosExps, text="Nombre", bg="#bddde4")
        self.textoExpsNombre.grid(row=0, column=0)
        self.cuadroExpsNombre = tk.Entry(self.frameDatosExps, textvariable=self.variableNombreExps)
        self.cuadroExpsNombre.grid(row=0,column=1)
        self.cuadroExpsNombre.configure(bg="#bddde4")

        self.variableProposito = tk.StringVar()
        self.textoLabsProposito = tk.Label(self.frameDatosExps, text="Proposito", bg="#bddde4")
        self.textoLabsProposito.grid(row=1, column=0)
        self.cuadroExpsProposito = tk.Entry(self.frameDatosExps, textvariable=self.variableProposito)
        self.cuadroExpsProposito.grid(row=1, column=1)
        self.cuadroExpsProposito.configure(bg="#bddde4")

        self.variableDuracion = tk.StringVar()
        self.textoExpsDuracion = tk.Label(self.frameDatosExps, text="Duracion", bg="#bddde4")
        self.textoExpsDuracion.grid(row=2, column=0)
        self.cuadroExpsDuracion = tk.Entry(self.frameDatosExps, textvariable=self.variableDuracion)
        self.cuadroExpsDuracion.grid(row=2, column=1)
        self.cuadroExpsDuracion.configure(bg="#bddde4")

        self.variableCientifico = tk.StringVar()
        self.textoExpsCientifico = tk.Label(self.frameDatosExps, text="Cientifico", bg="#bddde4")
        self.textoExpsCientifico.grid(row=3, column=0)
        self.cuadroExpsCientifico = tk.Entry(self.frameDatosExps, textvariable=self.variableCientifico)
        self.cuadroExpsCientifico.grid(row=3, column=1)
        self.cuadroExpsCientifico.configure(bg="#bddde4")

    #botones guardar, cancelar y nuevo de experimento 
    def botonesExps(self):
        self.buttonGuardarExps = tk.Button(self.frameBotonesExps, text="Guardar", command=self.funcionGuardarExps)
        self.buttonGuardarExps.grid(row=0, column=0, padx=10)
        self.buttonGuardarExps.config(relief="sunken", bd=3, bg="#606060")
        self.buttonCancelarExps = tk.Button(self.frameBotonesExps, text="Cancelar", command=self.funcionCancelarExps)
        self.buttonCancelarExps.grid(row=0, column=1,padx=10)
        self.buttonCancelarExps.config(relief="sunken", bd=3, bg="#606060")
        self.buttonNuevoExps = tk.Button(self.frameBotonesExps, text="Nuevo", command=self.funcionNuevoExps)
        self.buttonNuevoExps.grid(row=0, column=2, padx=10)
        self.buttonNuevoExps.config(relief="sunken", bd=3, bg="#606060")
    
    #funcion de los botones de laboratorio
    def funcionNuevoExps(self):
        self.variableNombreExps.set('')
        self.variableProposito.set('')
        self.variableDuracion.set('')
        self.variableCientifico.set('')
        self.cuadroExpsNombre.config(state='normal')
        self.cuadroExpsProposito.config(state='normal')
        self.cuadroExpsDuracion.config(state='normal')
        self.cuadroExpsCientifico.config(state='normal')
    
    def funcionCancelarExps(self):
        self.variableNombreExps.set('')
        self.variableProposito.set('')
        self.variableDuracion.set('')
        self.variableCientifico.set('')
        self.cuadroExpsNombre.config(state='disabled')
        self.cuadroExpsProposito.config(state='disabled')
        self.cuadroExpsDuracion.config(state='disabled')
        self.cuadroExpsCientifico.config(state='disabled')

    def funcionGuardarExps(self):
        self.experimento.nombre = self.variableNombreExps.get()
        self.experimento.proposito = self.variableProposito.get()
        self.experimento.duracion = self.variableDuracion.get()
        self.experimento.cientifico = self.variableCientifico.get()
        self.vincularBaseDatosExps()
        self.experimento.insertarBaseDatos()

    #vincular la base de datos laboratorio a la interfaz
    def vincularBaseDatosLabs(self):
        try:
            self.laboratorio.leerBaseDatos()
            self.tablaLabs.delete(*self.tablaLabs.get_children())
            for fila in self.laboratorio.Datos:
                self.tablaLabs.insert("", "end", values=fila)
        except:
            messagebox.showerror("Error", "No se actualizo la tabla de laboratorio")

    def vincularBaseDatosExps(self):
        try:
            self.experimento.leerBaseDatos()
            self.tablaExps.delete(*self.tablaExps.get_children())
            for fila in self.experimento.Datos:
                self.tablaExps.insert("", "end", values=fila)
        except:
            messagebox.showerror("Error", "No se actualizo la tabla de experimentos")

    def BotonesInferiores(self):
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonInferior, text="ID ")
        self.textoID.grid(row=0, column=0)
        self.textoID.config(relief="groove", bd=5, bg="grey")
        self.cuadroID = tk.Entry(self.frameBotonInferior, textvariable=self.variableID)
        self.cuadroID.grid(row=0, column=1, padx=5, pady=5, columnspan=2)
        self.cuadroID.configure(bg="grey")

        self.botonEditar = tk.Button(self.frameBotonInferior, text="Editar",relief="sunken", bd=3, bg="green", command=self.funcionEditar)
        self.botonEditar.grid(row=3,column=0, pady=5)

        self.botonEliminar = tk.Button(self.frameBotonInferior, text="Eliminar",relief="sunken", bd=3, bg="red", command=self.funcionEliminar)
        self.botonEliminar.grid(row=3,column=1, pady=5)

    def botonEliminarLabs(self):
        self.variableIDLabs = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonInferiorLabs, text="ID ")
        self.textoID.grid(row=0, column=0)
        self.textoID.config(relief="groove", bd=5, bg="grey")
        self.cuadroID = tk.Entry(self.frameBotonInferiorLabs, textvariable=self.variableIDLabs)
        self.cuadroID.grid(row=0, column=1, padx=5, pady=5, columnspan=2)
        self.cuadroID.configure(bg="grey")
        self.botonEliminar = tk.Button(self.frameBotonInferiorLabs, text="Eliminar",relief="sunken", bd=3, bg="red", command=self.funcionEliminarLabs)
        self.botonEliminar.grid(row=3,column=1, pady=5)


    def funcionEliminar(self):
        self.experimento.eliminarDatos("Experiments", self.variableID.get())
        messagebox.showinfo("Info", "Se borro exitosamente")
        self.vincularBaseDatosExps()
    
    def funcionEditar(self):
        if self.variableNombreExps.get():
            self.experimento.EditarBaseDatos("Experiments", "NOMBRE", self.variableNombreExps.get(), self.variableID.get())

        if self.variableDuracion.get():
            self.experimento.EditarBaseDatos("Experiments", "PROPOSITO", self.variableProposito.get(), self.variableID.get())
            
        self.vincularBaseDatosExps()

    def funcionEliminarLabs(self):
        self.laboratorio.eliminarDatos("Labs", self.variableIDLabs.get())
        messagebox.showinfo("Info", "Se borro exitosamente")
        self.vincularBaseDatosLabs()

aplicacion = Interfaz()