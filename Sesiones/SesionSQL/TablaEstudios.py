from SesionsSQL import BaseDeDatos

class  TablaEstudios(BaseDeDatos):
    def __init__(self):
        super().__init__()

    def CrearTablaEstudios(self):
        self.CrearTabla("Estudios", "ColumnaEstudios")

    def AgregarUnDatoE(self, carrera, fechainicio, fechafin):
        Datos=[
            (f"{carrera}", f"{fechainicio}", f"{fechafin}")
        ]
        self.InsertarDatos("Estudios", "?,?,?", Datos)