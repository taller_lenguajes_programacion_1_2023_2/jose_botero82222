import sqlite3
import json
class Conexion:
    def __init__(self):
        self.BaseDeDatos=sqlite3.connect("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/2doEntregable/DataBase.sqlite")
        self.Apuntador=self.BaseDeDatos.cursor()
        with open("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/2doEntregable/Queries.json", "r") as File:
            self.Querys=json.load(File)

    def CrearTabla(self, NombreTabla = "", Query = ""):
        if NombreTabla!="" and Query!="":
            Info=self.Querys["CrearTabla"].format(NombreTabla, self.Querys[Query])
            self.Apuntador.execute(Info)
            self.BaseDeDatos.commit()
        else:
            print("No puede dejar un campo vacio.")

    def InsertarDatos(self, Tabla="", interrogantes="", Values=""):
        if Tabla!="" and Values!="":
            Info=self.Querys["InsertarDatos"].format(Tabla, interrogantes)
            self.Apuntador.executemany(Info, Values)
            self.BaseDeDatos.commit()
            print("Datos ingresados")
        else:
            print("No puede dejar campos vacios")
    
    def LeerDatos(self, Tabla="", interrogantes="", values=""):
        if Tabla!="" and values!="":
            Info=self.Querys["LeerDatos"].format(Tabla,interrogantes)
            self.Apuntador.executemany(Info,values)
            self.BaseDeDatos.commit()
            print(Info)
            

    def ActualizarDatos(self, NuevoDato, Tabla ="", interrogantes="", values=""):
        if Tabla!="" and values!=0:           
            Info=self.Querys["ActualizarDatos"].format(Tabla,interrogantes ,NuevoDato)
            self.Apuntador.execute(Info)
            self.BaseDeDatos.commit()
        else:
            print("No puede dejar un campo vacio")
            return False
        
            
    def EliminarDatos(self, Tabla="", interrogantes="", values=""):
        if Tabla!="" and values!=0:
            Info=self.Querys["EliminarDatos"].format(Tabla, interrogantes)
            self.Apuntador.execute(Info, values)                    
            self.BaseDeDatos.commit()
            print("Dato borrado")
