from django.shortcuts import render


# Create your views here.
def paginaCategorias(request):
    return render(request, 'categorias.html')

def infoProductos(request):
    return render(request, 'infoProductos.html')

def paginaPerifericos(request):
    return render(request, 'perifericos.html')

def paginaComputadores(request):
    return render(request, 'computadores.html')