from Conexion import Conexion
class Experimento(Conexion):
    def __init__(self, nombre="", proposito="", duracion="", cientifico=""):
        Conexion.__init__(self)
        """| CONSTRUCTOR EXPERIMENTS |

        Args:
            Nombre (str): Nombre del experimento
            proposito (str): proposito por el cual se desarrolla el experimento.
            duracicion (str): Duracion del experimento.
            Cientifico (str): Cientificp responsable del experimento.
        """
        self.nombre = nombre
        self.proposito = proposito
        self.duracion = duracion
        self.cientifico= cientifico
        self.CrearBaseDatos()

    #funciones CRUD
    def CrearBaseDatos(self):
        self.crearTabla("Experiments", "Experimento")

    def insertarBaseDatos(self):
        datos=[
            (f"{self.nombre}",f"{self.proposito}",f"{self.duracion}",f"{self.cientifico}")
        ]
        self.insertarDato("Experiments", 4, datos)

    def leerBaseDatos(self):
        self.Datos=self.verTabla("Experiments")
    
    def EditarBaseDatos(self, nuevoDato, columna, id):
        if self.actualizarDatos(nuevoDato,"Experiments",columna,id):
            return True
        else:
            return False
 
    def eliminarBaseDatos(self, id):
        self.eliminarDatos("Experiments", id)



            