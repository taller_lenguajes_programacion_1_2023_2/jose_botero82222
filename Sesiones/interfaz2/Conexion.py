import sqlite3
import json

class Conexion:
    def __init__(self):
        self.baseDeDatos = sqlite3.connect("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/Sesiones/interfaz2/DataBase.sqlite")
        self.apuntador = self.baseDeDatos.cursor()
        with open("C:/Users/RICARDO/Documents/tallerlenguaje/jose_botero82222/Sesiones/interfaz2/Queries.json", "r") as queries:
            self.query = json.load(queries)

    def crearTabla(self, nombre, columnas):
        queryCrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.apuntador.execute(queryCrearTabla)
        self.baseDeDatos.commit()

    def insertarDato(self, tabla, columnas, datos):
        queryInsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.apuntador.executemany(queryInsertarDatos, datos)
        self.baseDeDatos.commit()

    def seleccionarTabla(self, tabla):
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(querySeleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido

    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla, columna,nuevovalor,'ID', id)
        self.apuntador.execute(queryActualizarDatos)
        self.baseDeDatos.commit()

    def eliminarDatos(self, tabla, id):
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla,'ID', id)
        self.apuntador.execute(queryEliminarDatos)
        self.baseDeDatos.commit()
        
    