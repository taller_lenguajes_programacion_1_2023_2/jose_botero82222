#Padre

from Excel import Excel
import pandas as pd
from Conexion import Conexion
class Pelicula(Excel, Conexion):
    def __init__(self, titulo="",  genero="", duracion=0.0, director="", estreno=""):
        Conexion.__init__(self)
        super().__init__()
        self.titulo=titulo
        self.genero=genero
        self.duracion=duracion
        self.director=director
        self.estreno=estreno  
        self.id=1      
        self.CrearBaseDatosPadre()
    
    def CrearBaseDatosPadre(self):
        self.CrearTabla("ClasePadre","Pelicula")
    
    def AgregarDatosPadre(self):
        DatosPadre=[
            (f"{self.titulo}",f"{self.genero}",f"{self.duracion}",f"{self.director}",f"{self.estreno}")

        ]
        self.InsertarDatos("Padre","?","?","?","?","?", DatosPadre)
    
         
    def getExcelPadre(self):
        """ Obtener la hoja de ClasePadre en el excel 
        
        Info:
            Imprimirá los datos actuales       
        """
        self.ClasePadre=self.LeerHoja("ClasePadre")
        if not self.ClasePadre.empty:
            print()
            print(self.ClasePadre.to_string(index=False))
        else:
            print("No hay datos.")  

    def setTitulo(self, nuevoTitulo):
        """ Cambiar titulo 

        Parametros:
            nuevoTitulo : String
        """
        self.titulo=nuevoTitulo

            
    def getTitulo(self):
        """ Obtener titulo 
        
        Return:
            titulo : String
        """
        return self.titulo
    
    def setGenero(self, nuevoGenero):
        """ Cambiar genero 

        Parametros:
            nuevoGenero : String
        """
        self.genero=nuevoGenero
    
    def getGenero(self):
        """ Obtener genero 
        
        Return:
            genero : String
        """
        return self.genero
    
    def getDuracion(self):
        """ Obtener duracion 
        
        Return:
            duracion : Float
        """
        return self.duracion

    def setDuracion(self, duracion):
        self.duracion = duracion

    def setDirector(self, director):
        self.precio=director
    
    def getDirector(self):
        """Obtener director 
        
        Return:
            director : String
        """
        return self.director
    
    def setEstreno(self, estreno):
        self.estreno=estreno
    
    def getEstreno(self):
        """Obtener estreno 
        
        Return:
            estreno : float
        """
        return self.estreno
    
