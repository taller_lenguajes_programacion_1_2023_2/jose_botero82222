from Estudio import Estudio
from Habilidad import Habilidad
from excel import Excel
class Persona:
    def __init__(self,primernombre,segundonombre,apellido,nacionalidad,telefono,email,tipo,nivel,id,titulo,inicio,fin,horas):
        self.primernombre=primernombre
        self.segundonombre=segundonombre
        self.apellido=apellido
        self.nacionalidad=nacionalidad
        self.telefono=telefono
        self.email=email
        self.habilidad=Habilidad(tipo,nivel,id)
        self.estudio=Estudio(titulo,inicio,fin,horas)
    def __str__(self):
        return f"Primer nombre: {self.primernombre}\n"\
            f"Segundo nombre: {self.segundonombre}\n"\
                f"Apellido: {self.apellido}\n"\
                    f"Nacionalidad: {self.nacionalidad}\n"\
                        f"Telefono: {self.telefono}\n"\
                            f"Email: {self.email}"
    def getHabilidad(self):
        print(self.habilidad)
    def getEstudio(self):
        print(self.estudio)
