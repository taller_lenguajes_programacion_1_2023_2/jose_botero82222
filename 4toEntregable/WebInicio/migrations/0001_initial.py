

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Personas',
            fields=[
                ('ID', models.IntegerField(primary_key=True, serialize=False)),
                ('NOMBRE', models.TextField(max_length=20)),
                ('APELLIDO', models.TextField(max_length=20)),
                ('USUARIO', models.TextField(max_length=20)),
                ('CLAVE', models.TextField(max_length=20)),
            ],
        ),
    ]
